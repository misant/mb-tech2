
### What is this repository for? ###

* Take Home Task
* Version 0.1

App is getting hosts public IP address, finds city by the IP and looks for temperature data using openweathermap.org API

### How to use ###

* Clone the repo
* Build container by executing build.sh from mb-tect2/docker/
* Start container by executing mb-tech2/docker/start.sh
* Run the app by executing mb-tech2/docker/exec.sh