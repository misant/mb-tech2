import requests
import json

weather_url = "http://api.openweathermap.org/data/2.5/weather"
ip_url = "http://ipinfo.io/"

# Get the public IP address
ip = requests.get("https://api.ipify.org").text

# Get the city for that IP
city = str(requests.get(ip_url+ip).json()["city"])

# Get weather data for that city
# API docs - https://openweathermap.org/current#cities
params = {
    'q': str(city),
    'appid': '11c0d3dc6093f7442898ee49d2430d20',
    'units': 'imperial'
}
weather = requests.get(weather_url, params=params).json()

print("""\nYour public IP is {} and according to my database its located in {}. 
Outside temperature is {}F and it feels like {}F. 
Minimal expected temperature = {}F.
Maximum expected temperature = {}F.
Average daily temperature = {:.2f}F.

Have a nice day!""".format(ip, city, weather["main"]["temp"], weather["main"]["feels_like"],
                           weather["main"]["temp_min"], weather["main"]["temp_max"],
                           (weather["main"]["temp_min"] + weather["main"]["temp_max"]) / 2 ))
